//! Implements a parser for the ArcSight CEF format, with a spec document at
//! https://drive.google.com/file/d/0B3_1xwpYF9W-M1hDUFBWQ1B3bHM/view

#[macro_use]
extern crate combine;

use combine::*;
use combine::char::{alpha_num, digit, string};
use combine::primitives::{Error, Info};

use std::collections::HashMap;

#[derive(PartialEq, Debug)]
pub struct CefRecord {
    version: u32,
    device_vendor: String,
    device_product: String,
    device_version: String,
    signature_id: String,
    name: String,
    severity: String, // maybe an enum
    extension: Option<HashMap<String, String>>
}

impl CefRecord {
    fn set_extension(self, extension: HashMap<String, String>) -> CefRecord {
        // XXX I'm hoping for compiler magic here, otherwise, redundant copies ahoy!
        CefRecord {
            version: self.version,
            device_vendor: self.device_vendor,
            device_product: self.device_product,
            device_version: self.device_version,
            signature_id: self.signature_id,
            name: self.name,
            severity: self.severity,
            extension: Some(extension)
        }
    }
}

fn cef_version<I>(input: I) -> ParseResult<u32, I>
    where I: Stream<Item = char>
{
    string("CEF:").
        with(many1(digit()).
             and_then(|s: String| s.parse::<u32>())).
        parse_stream(input)
}

fn prefix_value<I>(input: I) -> ParseResult<String, I>
    where I: Stream<Item = char>
{
    let escaped_pipe = string(r"\|").map(|_| '|');
    let escaped_escape = string(r"\\").map(|_| '\\');

    many1(choice!(
            try(escaped_pipe),
            try(escaped_escape),
            try(satisfy(|c| c != '|')))
        ).parse_stream(input)
}

fn prefix<I>(input: I) -> ParseResult<CefRecord, I>
    where I: Stream<Item = char>
{
    let prefix_values = token('|').with(
        count::<Vec<String>, _>(6, parser(prefix_value).skip(token('|'))).
        and_then(|values| {
            if values.len() != 6 {
                Err(Error::Message(Info::Borrowed("Prefixes are required to have 6 values")))
            } else {
                Ok(values)
            }
        }));

    parser(cef_version).and(prefix_values).
    map(|(version, values)|
        CefRecord {
            version: version,
            device_vendor: values[0].clone(),
            device_product: values[1].clone(),
            device_version: values[2].clone(),
            signature_id: values[3].clone(),
            name: values[4].clone(),
            severity: values[5].clone(),
            extension: None
        }
    ).parse_stream(input)
}

fn extension_key<I>(input: I) -> ParseResult<String, I>
    where I: Stream<Item = char>
{
    let key = many1(alpha_num());
    (key, look_ahead(token('='))).
        map(|(key, _)| key). // drop the =
        parse_stream(input)
}

fn extension_value<I>(input: I) -> ParseResult<String, I>
    where I: Stream<Item = char>
{
    let escaped_equals = string(r"\=").map(|_| '=');
    let escaped_cr = string(r"\r").map(|_| '\n');
    let escaped_lf = string(r"\n").map(|_| '\n');
    let escaped_escape = string(r"\\").map(|_| '\\');
    // XXX this has to perform terribly...
    let whitespace = token(' ').and(not_followed_by(parser(extension_key))).map(|(s, _)| s);
    let extension_chars = none_of(" =".chars());

    many1(choice!(
            try(escaped_cr),
            try(escaped_lf),
            try(escaped_equals),
            try(escaped_escape),
            try(whitespace),
            try(extension_chars))
        ).parse_stream(input)
}

fn extension_entry<I>(input: I) -> ParseResult<(String, String), I>
    where I: Stream<Item = char>
{
    (parser(extension_key), token('='), parser(extension_value), optional(token(' '))).
        map(|(key, _, value, _)| (key, value)).
        parse_stream(input)
}

fn extension<I>(input: I) -> ParseResult<HashMap<String, String>, I>
    where I: Stream<Item = char>
{
    many(parser(extension_entry)).
        map(|entries:Vec<(String, String)>| entries.into_iter().collect()).
        parse_stream(input)
}

pub fn cef_record<I>(input: I) -> ParseResult<CefRecord, I>
    where I: Stream<Item = char>
{
    (parser(prefix), parser(extension)).
        map(move |(record, extension)| record.set_extension(extension)).
        parse_stream(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cef_version_0() {
        let result = cef_version("CEF:0").unwrap().0;
        assert_eq!(result, 0);
    }

    #[test]
    fn cef_version_1_fail() {
        let result = cef_version("CEF:1").unwrap().0;
        assert_ne!(result, 0);
    }

    #[test]
    fn reject_non_cef_prefix() {
        let result = cef_version("BAD:0");
        assert!(result.is_err());
    }

    #[test]
    fn cef_version_parse_fail() {
        let result = cef_version("CEF:12345678901234567890");
        assert!(result.is_err());
    }

    #[test]
    fn prefix_value_simple() {
        let result = prefix_value("Device Vendor").unwrap().0;
        assert_eq!(result, "Device Vendor");
    }

    #[test]
    fn prefix_value_escaped_pipe() {
        let result = prefix_value(r"Device\|Vendor").unwrap().0;
        assert_eq!(result, "Device|Vendor");
    }

    #[test]
    fn prefix_incomplete() {
        let text = "CEF:0|vendor|product|version|sig|name|";
        let result = prefix(text);
        assert!(result.is_err());
    }

    #[test]
    fn prefix_simple() {
        let text = "CEF:0|vendor|product|version|sig|name|sev|extension";
        let result = prefix(text).unwrap();

        let (parsed, remaining) = (result.0, result.1);
        assert_eq!(parsed, CefRecord { 
            version: 0,
            device_vendor: "vendor".into(),
            device_product: "product".into(),
            device_version: "version".into(),
            signature_id: "sig".into(),
            name: "name".into(),
            severity: "sev".into(),
            extension: None 
        });
        assert_eq!(remaining.into_inner(), "extension");
    }

    #[test]
    fn extension_value_simple() {
        let result = extension_value(r"c6a1Label").unwrap().0;
        assert_eq!(result, "c6a1Label");
    }

    #[test]
    fn extension_value_escaped_equals() {
        let result = extension_value(r"blocked a \=").unwrap().0;
        assert_eq!(result, "blocked a =");
    }

    #[test]
    fn extension_value_escaped_escape() {
        let result = extension_value(r"blocked a \\").unwrap().0;
        assert_eq!(result, "blocked a \\");
    }

    #[test]
    fn extension_value_multiline() {
        let result = extension_value(r"Detected a threat.\r No action needed.").unwrap().0;
        assert_eq!(result, "Detected a threat.\n No action needed.");
    }

    #[test]
    fn extension_entry_simple() {
        let result = extension_entry(r"foo=bar").unwrap().0;
        assert_eq!(result, ("foo".to_owned(), "bar".to_owned()));
    }

    #[test]
    fn extension_entry_complicated() {
        let result = extension_entry(r"lameKey=terrible\\no|good very\rbad\nno good\=value   ").unwrap().0;
        assert_eq!(result, ("lameKey".to_owned(), "terrible\\no|good very\nbad\nno good=value   ".to_owned()));
    }

    #[test]
    fn extension_empty() {
        let result = extension(r"").unwrap().0;
        assert_eq!(result, [].iter().cloned().collect::<HashMap<String, String>>());
    }

    #[test]
    fn extension_simple() {
        let result = extension(r"foo=bar baz=snork").unwrap().0;
        assert_eq!(result, 
                   [ ("foo".into(), "bar".into()), ("baz".into(), "snork".into()) ].
                   iter().cloned().collect::<HashMap<String, String>>()
                   );
    }

    #[test]
    fn extension_moderate() {
        let result = extension(r"src=10.0.0.1 msg=Detected a threat.\n No action needed.").unwrap().0;
        assert_eq!(result, [ 
                   ("src".into(), "10.0.0.1".into()),
                   ("msg".into(), "Detected a threat.\n No action needed.".into()), ].
                   iter().cloned().collect::<HashMap<String, String>>()
                   );
    }

    #[test]
    fn extension_messy() {
        let result = extension(r"lameKey=terrible\\no|good  very\rbad\nno good\=value   foo=bar baz=snork").unwrap().0;
        assert_eq!(result, [ 
                   ("lameKey".to_owned(), "terrible\\no|good  very\nbad\nno good=value  ".to_owned()),
                   ("foo".into(), "bar".into()), 
                   ("baz".into(), "snork".into()) ].
                   iter().cloned().collect::<HashMap<String, String>>()
                   );
    }

    #[test]
    fn complete_record_1() {
        let input = State::new(r"CEF:0|security|threatmanager|1.0|100|Detected a threat. No action needed.|10|src=10.0.0.1 msg=Detected a threat.\n No action needed.");

        let result = cef_record(input).unwrap();
        let (parsed, remaining) = (result.0, result.1);

        assert_eq!(parsed, CefRecord { 
            version: 0,
            device_vendor: "security".into(),
            device_product: "threatmanager".into(),
            device_version: "1.0".into(),
            signature_id: "100".into(),
            name: "Detected a threat. No action needed.".into(),
            severity: "10".into(),
            extension: Some(
                [
                    ("src".to_owned(), "10.0.0.1".to_owned()),
                    ("msg".into(), "Detected a threat.\n No action needed.".into())
                ].iter().cloned().collect::<HashMap<String, String>>())
        });
        assert_eq!(remaining.into_inner().input, "");
    }

}
